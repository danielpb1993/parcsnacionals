package com.example.flip_swap;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ImageView imageView5;

    private LinearLayout linearText1;
    private LinearLayout linearText2;
    private LinearLayout linearText3;
    private LinearLayout linearText4;
    private LinearLayout linearText5;

    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;

    private boolean clicked1 = false;
    private boolean clicked2 = false;
    private boolean clicked3 = false;
    private boolean clicked4 = false;
    private boolean clicked5 = false;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = findViewById(R.id.imageView1);
        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        imageView4 = findViewById(R.id.imageView4);
        imageView5 = findViewById(R.id.imageView5);

        linearText1 = findViewById(R.id.linearText1);
        linearText2 = findViewById(R.id.linearText2);
        linearText3 = findViewById(R.id.linearText3);
        linearText4 = findViewById(R.id.linearText4);
        linearText5 = findViewById(R.id.linearText5);

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);

        button1.setVisibility(View.GONE);
        button2.setVisibility(View.GONE);
        button3.setVisibility(View.GONE);
        button4.setVisibility(View.GONE);
        button5.setVisibility(View.GONE);

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clicked1) {
                    //dona la volta a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView1, "RotationY", 0, 180);
                    objectAnimator1.setDuration(1000);

                    //disminueix la opacitat de la imatge
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView1, "alpha",1f, 0.5f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    //fa que aparegui el text i el botó. De -180 a 0 per a que no aparegui al revés
                    objectAnimator3 = ObjectAnimator.ofFloat(linearText1, "RotationY", -180, 0);
                    objectAnimator3.setDuration(1000);

                    //fa que desaparegui el text
                    objectAnimator4 = ObjectAnimator.ofFloat(linearText1, "alpha", 0f, 1f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    button1.setVisibility(View.VISIBLE);
                    clicked1 = true;
                } else {
                    //torna a la posició inicial a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView1, "RotationY", 180, 0);
                    objectAnimator1.setDuration(1000);

                    objectAnimator2 = ObjectAnimator.ofFloat(imageView1, "alpha", 0.5f, 1f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    objectAnimator3 = ObjectAnimator.ofFloat(linearText1, "RotationY", 0, -180);
                    objectAnimator3.setDuration(1000);

                    objectAnimator4 = ObjectAnimator.ofFloat(linearText1, "alpha", 1f, 0f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            button1.setVisibility(View.GONE);
                        }
                    }, 500);
                    clicked1 = false;
                }

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clicked2) {
                    //dona la volta a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView2, "RotationY", 0, 180);
                    objectAnimator1.setDuration(1000);

                    //disminueix la opacitat de la imatge
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView2, "alpha",1f, 0.5f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    //fa que aparegui el text i el botó. De -180 a 0 per a que no aparegui al revés
                    objectAnimator3 = ObjectAnimator.ofFloat(linearText2, "RotationY", -180, 0);
                    objectAnimator3.setDuration(1000);

                    //fa que desaparegui el text
                    objectAnimator4 = ObjectAnimator.ofFloat(linearText2, "alpha", 0f, 1f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    button2.setVisibility(View.VISIBLE);
                    clicked2 = true;
                } else {
                    //torna a la posició inicial a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView2, "RotationY", 180, 0);
                    objectAnimator1.setDuration(1000);

                    objectAnimator2 = ObjectAnimator.ofFloat(imageView2, "alpha", 0.5f, 1f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    objectAnimator3 = ObjectAnimator.ofFloat(linearText2, "RotationY", 0, -180);
                    objectAnimator3.setDuration(1000);

                    objectAnimator4 = ObjectAnimator.ofFloat(linearText2, "alpha", 1f, 0f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            button2.setVisibility(View.GONE);
                        }
                    }, 500);
                    clicked2 = false;
                }

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clicked3) {
                    //dona la volta a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView3, "RotationY", 0, 180);
                    objectAnimator1.setDuration(1000);

                    //disminueix la opacitat de la imatge
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView3, "alpha",1f, 0.5f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    //fa que aparegui el text i el botó. De -180 a 0 per a que no aparegui al revés
                    objectAnimator3 = ObjectAnimator.ofFloat(linearText3, "RotationY", -180, 0);
                    objectAnimator3.setDuration(1000);

                    //fa que desaparegui el text
                    objectAnimator4 = ObjectAnimator.ofFloat(linearText3, "alpha", 0f, 1f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    button3.setVisibility(View.VISIBLE);
                    clicked3 = true;
                } else {
                    //torna a la posició inicial a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView3, "RotationY", 180, 0);
                    objectAnimator1.setDuration(1000);

                    objectAnimator2 = ObjectAnimator.ofFloat(imageView3, "alpha", 0.5f, 1f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    objectAnimator3 = ObjectAnimator.ofFloat(linearText3, "RotationY", 0, -180);
                    objectAnimator3.setDuration(1000);

                    objectAnimator4 = ObjectAnimator.ofFloat(linearText3, "alpha", 1f, 0f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            button3.setVisibility(View.GONE);
                        }
                    }, 500);
                    clicked3 = false;
                }

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clicked4) {
                    //dona la volta a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView4, "RotationY", 0, 180);
                    objectAnimator1.setDuration(1000);

                    //disminueix la opacitat de la imatge
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView4, "alpha",1f, 0.5f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    //fa que aparegui el text i el botó. De -180 a 0 per a que no aparegui al revés
                    objectAnimator3 = ObjectAnimator.ofFloat(linearText4, "RotationY", -180, 0);
                    objectAnimator3.setDuration(1000);

                    //fa que desaparegui el text
                    objectAnimator4 = ObjectAnimator.ofFloat(linearText4, "alpha", 0f, 1f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    button4.setVisibility(View.VISIBLE);
                    clicked4 = true;
                } else {
                    //torna a la posició inicial a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView4, "RotationY", 180, 0);
                    objectAnimator1.setDuration(1000);

                    objectAnimator2 = ObjectAnimator.ofFloat(imageView4, "alpha", 0.5f, 1f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    objectAnimator3 = ObjectAnimator.ofFloat(linearText4, "RotationY", 0, -180);
                    objectAnimator3.setDuration(1000);

                    objectAnimator4 = ObjectAnimator.ofFloat(linearText4, "alpha", 1f, 0f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            button4.setVisibility(View.GONE);
                        }
                    }, 500);
                    clicked4 = false;
                }

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });

        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clicked5) {
                    //dona la volta a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView5, "RotationY", 0, 180);
                    objectAnimator1.setDuration(1000);

                    //disminueix la opacitat de la imatge
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView5, "alpha",1f, 0.5f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    //fa que aparegui el text i el botó. De -180 a 0 per a que no aparegui al revés
                    objectAnimator3 = ObjectAnimator.ofFloat(linearText5, "RotationY", -180, 0);
                    objectAnimator3.setDuration(1000);

                    //fa que desaparegui el text
                    objectAnimator4 = ObjectAnimator.ofFloat(linearText5, "alpha", 0f, 1f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    button5.setVisibility(View.VISIBLE);
                    clicked5 = true;
                } else {
                    //torna a la posició inicial a la imatge
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView5, "RotationY", 180, 0);
                    objectAnimator1.setDuration(1000);

                    objectAnimator2 = ObjectAnimator.ofFloat(imageView5, "alpha", 0.5f, 1f);
                    objectAnimator2.setStartDelay(500);
                    objectAnimator2.setDuration(1);

                    objectAnimator3 = ObjectAnimator.ofFloat(linearText5, "RotationY", 0, -180);
                    objectAnimator3.setDuration(1000);

                    objectAnimator4 = ObjectAnimator.ofFloat(linearText5, "alpha", 1f, 0f);
                    objectAnimator4.setStartDelay(500);
                    objectAnimator4.setDuration(1);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            button5.setVisibility(View.GONE);
                        }
                    }, 500);
                    clicked5 = false;
                }

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                animatorSet.start();
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });
    }
}